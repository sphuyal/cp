<?php
class Event_model extends CI_Model
{
    public function ___construct()
{
    $this->load->database();
}

public function get_event($slug = FALSE){
    if($slug ===FALSE){
        $query = $this->db->get('tbl_event');
        return $query->result_array();
    }

    $query = $this->db->get_where('tbl_event', array('slug' => $slug));
    return $query->row_array();
}

public function create_event(){
    $slug = url_title($this->input->post('title'), 'dash', TRUE);

    $data = array(
        'event_date' => $this->input->post('event_date'),
        'event_time' => $this->input->post('event_time'),
        'title' => $this->input->post('title'),
        'slug' => $slug,
        'body' => $this->input->post('body')

    );
    return $this->db->insert('tbl_event',$data);
}

public function delete_event($id){
    $this->db->where('id',$id);
    $this->db->delete('tbl_event');
    return true;
}

public function eventid($title,$slug)
{

        $this->db->select('id');
    $this->db->where('title',$title);
        $this->db->where('slug',$slug);
        return $this->db->get('tbl_event');
}


}
