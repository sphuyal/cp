<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] ='home';

$route['register']='register/register';
$route['admindashboard']='admin/index';
$route['staffdashboard']='staff/index';
$route['trainerdashboard']='trainer/index';
$route['customerdashboard']='customer/index';
$route['Eventpost']='Eventpost/create';
$route['event']='Eventpost/index';
$route['viewevent']='viewevent/index';
$route['viewevent/(:any)']='viewevent/view/$1';
$route['event/(:any)'] ='Eventpost/view/$1';
$route['Event'] ='Eventpost/delete';
$route['detail'] ='Detail/index';
$route['create-student'] = 'Detail/create_user_info';
$route['view-user-info/(:num)'] = 'Detail/view_user_info/$1';
$route['update-user-info/(:num)'] = 'Detail/update_user_info/$1';
$route['delete-user-info/(:num)'] = 'Detail/delete_user_info/$1';
$route['subscribe']='subscribe/index/$1';
$route['404_override'] = 'not_found';
$route['translate_uri_dashes'] = FALSE;
