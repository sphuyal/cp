<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Default Controller
 */
class Contact extends CI_Controller
{
	public function index()
	{
		$this->load->view('contact');
	}
}