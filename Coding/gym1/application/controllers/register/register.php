<?php
class register extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	  }

	


	  public function index() {
			
		/* Load form validation library */ 
		$this->load->library('form_validation');
		   
	/* Validation rule */
	$this->form_validation->set_rules('name', 'Name', 'required');
	$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_check_user');
	$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[15]');
	$this->form_validation->set_rules('choose', 'Choose', 'required');	
	$this->form_validation->set_rules('gender', 'Gender', 'required');
	$this->form_validation->set_rules('address', 'Address', 'required');
	$this->form_validation->set_rules('phonenumber', 'Phonenumber', 'required');
	$this->form_validation->set_rules('username', 'Username', 'required');
		   
		if ($this->form_validation->run() == FALSE) { 
		   $this->load->view('register/register'); 
		} 
		else { 
		   $this->load->model('register_model');
		   $this->register_model->saveuser();
		   $success = "Your account has been successfully created!";
		   $this->load->view('register/register', compact('success')); 
		} 
	 }
	 public function check_user($email)
	  {
			$query = $this->db->where('email', $email)->get("tbl_users");
		if ($query->num_rows() > 0)
		   {
			$this->form_validation->set_message('check_user','The '.$email.' belongs to an existing account');
			   return FALSE;
		   }
		 else 
			 return TRUE;
	 }	
}
 
?>