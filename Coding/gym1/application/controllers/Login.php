<?php
class Login extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->load->model('login_model');
  }

  function index(){
    $this->load->view('login_view');
  }

  function auth(){
    $username = $this->input->post('username',TRUE);
    $password = ($this->input->post('password',TRUE));
    $validate = $this->login_model->validate($username,$password);
    if($validate->num_rows() > 0){
        $data  = $validate->row_array();
        $id   =$data['id'];
        $name  = $data['name'];
        $email = $data['email'];
        $choose = $data['choose'];
        $gender = $data['gender'];
        $address = $data['address'];
        $phonenumber = $data['phonenumber'];
        $username = $data['username'];
        $password =$data['password'];
        $sesdata = array(
            'id'        =>$id,
            'name'      => $name,
            'email'     => $email,
            'choose'    => $choose,
            'gender'    => $gender,
            'address'   =>$address,
            'phonenumber' =>$phonenumber,
            'username'  =>$username,
            'password'  =>$password, 
            'logged_in' => TRUE
        );
        $this->session->set_userdata($sesdata);
        // access login for admin
        if($choose === 'admin'){
          redirect(base_url('Admin/index'));

            


        // access login for staff
        }elseif($choose === 'staff'){
            redirect('Staff/index');   

         // access login for staff
        }elseif($choose === 'trainer'){
          redirect('Trainer/index');

        // access login for customer
        }else{
            redirect('Customer/index');
        }
    }else{
        echo $this->session->set_flashdata('msg','Username or Password is Wrong');
        redirect('login');
    }


  


  }

  function logout(){
      $this->session->sess_destroy();
      redirect('login');
  }

}
