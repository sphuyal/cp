<?php
class Admin extends CI_Controller{
  function __construct(){
    parent::__construct();
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');}
      $this->load->model('Dashboard_model');
			$this->load->database();
    
  }
 
     
  function index(){
   
    
      if($this->session->userdata('choose')==='admin'){
        $data['detail']=$this->Dashboard_model->getdetails();
        $this->load->view('dashboard_view',$data);
        $this->load->view('admindashboard',$data); 
        $this->load->view('f',$data); 
        
      }else{
          echo "Access Denied";
	  } 

  }


  public function edit()
		{
			$name=$this->input->post('name');
			$email=$this->input->post('email');
			$gender=$this->input->post('gender');
			$address=$this->input->post('address');
			$phonenumber=$this->input->post('phonenumber');
			$username=$this->input->post('username');
			$password=$this->input->post('password');

			$data=array
			([
				
			]);

			$this->Dashboard_model->update([
        'name'      => $name,
        'email'     => $email,
        'gender'    => $gender,
        'address'   =>$address,
        'phonenumber' =>$phonenumber,
        'username'  =>$username,
        'password'  =>$password,   
			]);
			$data=array(
			'update'=>'Updated successfully');
			$this->session->set_flashdata($data);
			
			redirect('admindashboard');

		}
}
?>