<?php
class subscribe extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	  }

	


	  public function index() {
			
		/* Load form validation library */ 
		$this->load->library('form_validation');
		   
	/* Validation rule */
	
	$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_check_user');
	
		   
		if ($this->form_validation->run() == FALSE) { 
			$this->load->view('login_view');
			  



		} 
		else { 
		   $this->load->model('subscribe_model');
		   $this->subscribe_model->sub();
		   $success = "you are subscribed!";
		   $this->load->view('login_view'); 
		} 
	 }
	 public function check_user($email)
	  {
			$query = $this->db->where('email', $email)->get("tbl_subscribe");
		if ($query->num_rows() > 0)
		   {
			$this->form_validation->set_message('check_user','The '.$email.' belongs to an existing account');
			   return FALSE;
		   }
		 else 
			 return TRUE;
	 }	
}
 
?>