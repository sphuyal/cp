<?php
	class viewevent extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('Event_model');
		  }
		  public function index(){

		$data['title'] ='Our Events';

		$data['events'] =$this->Event_model->get_event();
		
        $this->load->view('dashboard_view',$data);
		$this->load->view('viewevent',$data);
		$this->load->view('f',$data);
		  }

		  public function view($slug = NULL){
			  $data['event'] = $this->Event_model->get_event($slug);

			  if(empty($data['event'])){
				  show_404();
			  }

			  $data['title']=$data['event']['title'];

			  $this->load->view('dashboard_view',$data);
			  $this->load->view('event_view',$data);
			  $this->load->view('f',$data);
		  }
		  public function create(){
			$this->load->library('form_validation');

			  $data['title'] = 'Post Event';
			  $this->form_validation->set_rules('event_date', 'Event-Date', 'required');
			  $this->form_validation->set_rules('event_time', 'Event-Time', 'required');	
			  $this->form_validation->set_rules('title', 'Title', 'required');
			  $this->form_validation->set_rules('body', 'Body', 'required');

			  if($this->form_validation->run() === False){

			  $this->load->view('dashboard_view',$data);
			  $this->load->view('event_view',$data);
			  $this->load->view('f', $data);
			  }else{
				  $this->Event_model->create_event();
				 redirect('event_view');
			  }

          }
          
        }