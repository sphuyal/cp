<?php
Class Testunit extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
        $this->load->library("unit_test");
		$this->load->model("Event_model");
		$this->load->model("Login_model");
		$this->load->model("Dashboard_model");
		$this->load->model("Complain_model");
    }


    private function delete_event($id)
	{
		return $this->Event_model->delete_event($id);
	}


	private function getid($username,$password)
	{
		$result=$this->Login_model->getid($username,($password))->row();
		return (int)$result->id;
		
	} 


	public function eventid($title,$slug)
	{
		$result=$this->Event_model->eventid($title,($slug))->row();
		return (int)$result->id;
		
	} 



	public function complainid($c_id)
	{
		$result=$this->Complain_model->complainid($c_id)->row();
		return (int)$result->id;
		
	} 


	public function email($email)
	{
		$result=$this->subscribe_model->email($email)->row();
		return (int)$result->id;
		
	} 


	



    public function index()
	{
		//for getting if login condition matches
		echo "<h2><u>1.Delete Event Unit Testing Success</u></h2>";
		$test1=$this->delete_event(1);
		$expected1=TRUE;
		$test_name1="Delete Event ";
		echo $this->unit->run($test1,$expected1,$test_name1);

		//for getting if Delete condition matches
		echo "<h2><u>2.Delete Event Unit Testing Failed</u></h2>";
		$test2=$this->delete_event(1);
		$expected2=FALSE;
		$test_name2="Delete Event ";
		echo $this->unit->run($test2,$expected2,$test_name2);




        //for getting if login condition matches
		echo "<h2><u>3.Unit testing of Customer detail (Success)</u></h2>";
		$test3=$this->getid("p1","kingdom123"); 
		$expected3=17;
		$test_name3="Get Customer ID";
		echo $this->unit->run($test3,$expected3,$test_name3);



		//for getting if login condition matches
		echo "<h2><u>4.Unit testing of Customer detail(Failed)</u></h2>";
		$test4=$this->getid("p1","kingdom123"); 
		$expected4=5;
		$test_name4="Get Customer ID Failed";
		echo $this->unit->run($test4,$expected4,$test_name4);



		//for getting if login condition matches
		echo "<h2><u>5.Unit testing of Admin detail(Success)</u></h2>";
		$test9=$this->getid("fik","123"); 
		$expected9=1;
		$test_name9="Get Admin ID";
		echo $this->unit->run($test9,$expected9,$test_name9);


		//for getting if login condition matches
		echo "<h2><u>6.Unit testing of Admin detail (Failed)</u></h2>";
		$test10=$this->getid("fik","123"); 
		$expected10=2;
		$test_name10="Get Admin ID Failed";
		echo $this->unit->run($test10,$expected10,$test_name10);

		//for getting if login condition matches
		echo "<h2><u>7.Unit testing of Staff detail (Success) </u></h2>";
		$test11=$this->getid("s0","kingdom123"); 
		$expected11=16;
		$test_name11="Get Staff ID";
		echo $this->unit->run($test11,$expected11,$test_name11);


		//for getting if login condition matches
		echo "<h2><u>8.Unit testing of Staff detail (Failed)</u></h2>";
		$test12=$this->getid("s0","kingdom123"); 
		$expected12=18;
		$test_name12="Get Staff ID Failed";
		echo $this->unit->run($test12,$expected12,$test_name12);




		//for getting if login condition matches
		echo "<h2><u>9.Unit testing of Trainer detail (Success)</u></h2>";
		$test13=$this->getid("phuyal31","kingdom123"); 
		$expected13=6;
		$test_name13="Get Trainer ID";
		echo $this->unit->run($test13,$expected13,$test_name13);


		//for getting if login condition matches
		echo "<h2><u>10.Unit testing of Trainer detail (Failed)</u></h2>";
		$test14=$this->getid("phuyal31","kingdom123"); 
		$expected14=7;
		$test_name14="Get Trainer ID Failed";
		echo $this->unit->run($test14,$expected14,$test_name14);


		



		//for getting if login condition matches
		echo "<h2><u>11.Unit Testing of Event detail (Success)</u></h2>";
		$test5=$this->eventid("Event in banasthali","event-in-banasthali"); 
		$expected5=17;
		$test_name5="Get Event ID ";
		echo $this->unit->run($test5,$expected5,$test_name5);


		//for getting if login condition matches
		echo "<h2><u>12.Unit Testing of Event detail (Failed)</u></h2>";
		$test6=$this->eventid("Event in banasthali","event-in-banasthali"); 
		$expected6=18;
		$test_name6="Get Event ID Failed ";
		echo $this->unit->run($test6,$expected6,$test_name6);



		//for getting if login condition matches
		echo "<h2><u>13.Unit Testing of Complain detail (Success)</u></h2>";
		$test7=$this->complainid("3"); 
		$expected7=5;
		$test_name7=" Get Complain ID ";
		echo $this->unit->run($test7,$expected7,$test_name7);




		//for getting if login condition matches
		echo "<h2><u>14.Unit Testing of Complains (Failed)</u></h2>";
		$test8=$this->complainid("3"); 
		$expected8=6;
		$test_name8="Get Complain ID Failed";
		echo $this->unit->run($test8,$expected8,$test_name8);



		

		
        
    }


}
