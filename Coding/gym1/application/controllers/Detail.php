<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Detail extends CI_Controller
{


    public function index() {

        $this->load->library('pagination');
        $config['per_page'] = 10;

        $page = $this->input->get('page', true);
        $search_data = $this->input->get('search_data', true);

        if ($search_data != '') {

            $this->db->like('name', $search_data);
            $this->db->or_like('address', $search_data);
            $this->db->or_like('choose', $search_data);
            $this->db->or_like('gender', $search_data);
            $this->db->or_like('address', $search_data);
            $this->db->or_like('phonenumber', $search_data);
        }
        $tempdb = clone $this->db;
        $total_row = $tempdb->from('tbl_users')->count_all_results();
        $this->db->limit($config['per_page'], $page);
        $this->db->order_by("id", "desc");
        $result['student_list'] = $this->db->get('tbl_users')->result_array();

        $config['base_url'] = "http://localhost:82/gym1/detail?search_data=$search_data";
        $config['total_rows'] = $total_row;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['query_string_segment'] = 'page';


        //Optional (Pagination Design)
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['prev_link'] = '&lt;Previous';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next &gt;</i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';


        $this->pagination->initialize($config);
        $result['pagination'] = $this->pagination->create_links();


        $data['content'] = $this->load->view('User_list', $result, true);
        $this->load->view('dashboard_view',$data);
        $this->load->view('detail', $data);
    }



    public function create_user_info() {


        if (isset($_POST['name'])) {
            $post_data = $_POST;
            $result = $this->db->insert('tbl_users', $post_data);
            if ($result) {
                $this->session->set_flashdata('message', 'Succefully Created User Info.');
            } else {
                $this->session->set_flashdata('message', "An error occurred while inserting data.");
            }
            redirect('detail');
        }
    }


    public function view_user_info($id) {
        if ($id != '') {
            $this->db->where('id', $id);
            $row['student_info'] = $this->db->get('tbl_users')->row_array();
        }
        $data['content'] = $this->load->view('view_user_info', $row, true);
        $this->load->view('dashboard_view',$data);
        $this->load->view('detail', $data);
        $this->load->view('f', $data);
    }




    public function update_user_info($id) {


        if (isset($_POST['id'])) {
            $update_data = $_POST;
            $id = $update_data['id'];
            unset($update_data['id']);

            $this->db->where('id', $id);
            $result = $this->db->update('tbl_users', $update_data);

            if ($result) {

                $this->session->set_flashdata('message', 'Succefully Updated User Info.');
            } else {
                $this->session->set_flashdata('message', 'An error occurred while inserting data');
            }

            //  redirect('student');
        }
        if ($id != '') {
            $this->db->where('id', $id);
            $row['student_info'] = $this->db->get('tbl_users')->row_array();
        }
        $data['content'] = $this->load->view('update-user-info', $row, true);
        $this->load->view('dashboard_view',$data);
        $this->load->view('detail', $data);
        $this->load->view('f', $data);
    }



    
    public function delete_user_info($id) {

        if ($id != '') {
            $this->db->where('id', $id);
            $result = $this->db->delete('tbl_users');
            if ($result) {
                $this->session->set_flashdata('message', 'Succefully Deleted User Info.');
            } else {
                $this->session->set_flashdata('message', "An error occurred while inserting data.");
            }
            redirect('detail');
        }
    }

}