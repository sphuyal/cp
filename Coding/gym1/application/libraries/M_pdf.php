<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once FCPATH.'/vendor/autoload.php';

/**
 * MPDF Class For Generating the pdf
 */

class M_pdf
{
    public $pdf;
 
    // public function __construct()
    // {
    //     $this->pdf = new \Mpdf\Mpdf(['orientation' => 'L']);
    // }

    public function configration($config)
    {
        $this->pdf = new \Mpdf\Mpdf();
    }
}