<?php $this->load->view('header'); ?> 
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css/fonts/adminlogin.css');?>" >
<div id="fh5co-wrapper">  </div>
<div id="fh5co-page"></div>
<?php $this->load->view('menu'); ?>
<!-- end:fh5co-header -->
<div class="fh5co-hero" style="background-color:#0d0e11;height:100px">
</div>


<div class="container">
        <div class="row">
            <div class="col-sm-offset-3 col-sm-6" id="login-form">
                <div class="col-sm-12">
                    <h1 class="text-center">Admin Login</h1>
                </div>
                <?php if($this->session->flashdata('errors'))
                {
                    echo '<div class="alert alert-warning">';
            echo $this->session->flashdata('errors');
            echo "</div>";
                }
if($this->session->flashdata('login_fail'))
                {
                    echo '<div class="alert alert-warning">';
            echo $this->session->flashdata('login_fail');
            echo "</div>";
                }
                
      ?>

                <form method="post" action="<?php echo base_url('Adminlogin/check')?>">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label" for="name" id="usrn">
                                Username
                            </label>
                            <input class="form-control" name="username" type="text" />
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="name" id="pwdn">
                                Password
                            </label>
                            <input class="form-control" name="password" type="Password" />
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Login" id="btm">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


  


<?php $this->load->view('footer'); ?> 