<?php if($this->session->userdata('id')) ?>
<h2 align="center">All Complains</h2>
<?php foreach ($complains as $row) : ?>
<div class="container">
        <div class="row">
            <div class="alert alert-warning" style="background-color:#d7bd94;">
                <u>
                    <div class="col-md-6"><b>Complain Date: <?php echo $row['date']?></b></div>
                </u> <br>
                <div class="alert alert-success">
                <label><b>Complain By: <u><?php echo $row['name']?></u></b></label>
                </div>
                <div class="alert alert-info">
                    <h4 align="justify"><?php echo $row['complains'] ?> </h4>
                </div>
               
            </div>

        </div>
    </div>

<?php endforeach; ?>