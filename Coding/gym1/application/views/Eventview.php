<?php $this->load->view('header'); ?> 
<h2 align="center"><?= $title ?></h2>
<div class="container">
    <div class="row">
        <div class="alert alert-warning">
            <u>
                <div class="col-md-6"><b>Event-Date: <?php echo $event['event_date']?></b></div>
                <div class="col-md-6"><span class="pull-right"><b>Event-Time:<?php echo $event['event_time']?></b></span></div>
            </u> <br>
            
            <div class="alert alert-info">
                <h4><?php echo $event['body']?> </h4>
            </div>
                <?php echo form_open('/Eventpost/delete/' .$event['id']); ?>
                <input type="submit"  value="delete" class="btn btn-danger">
        </div>

    </div>
</div>