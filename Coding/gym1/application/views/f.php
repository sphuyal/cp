<!-- fh5co-blog-section -->


<footer>
			<div id="footer">
				<div class="container">


					<div class="row">
						
						
					</div>
					<div class="row copy-right">
						<div class="col-md-6 col-md-offset-3 text-center">
							<p class="fh5co-social-icons">
								<a href="https://www.twitter.com"><i class="icon-twitter2"></i></a>
								<a href="https://www.facebook.com"><i class="icon-facebook2"></i></a>
								<a href="https://www.instragram.com"><i class="icon-instagram"></i></a>
								<a href="https://www.youtube.com"><i class="icon-youtube"></i></a>
							</p>
							<p>Copyright 2019 <a href="#">Fitness</a>. All Rights Reserved. <br>Made with <i class="icon-heart3"></i> by </p>
						</div>
					</div>
				</div>
			</div>
		</footer>
	

	</div>
	<!-- END fh5co-page -->

	</div>
	<!-- END fh5co-wrapper -->

	<!-- jQuery -->


	<!-- <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script> -->
	<script src="<?php echo base_url ('assets/front/js/jquery.min.js');?>"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_url ('assets/front/js/jquery.easing.1.3.js');?>"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url ('assets/front/js/bootstrap.min.js');?>"></script>
	<!-- Waypoints -->
	<script src="<?php echo base_url ('assets/front/js/jquery.waypoints.min.js');?>"></script>
	<!-- Stellar -->
	<script src="<?php echo base_url ('assets/front/js/jquery.stellar.min.js');?>"></script>
	<!-- Superfish -->
	<script src="<?php echo base_url ('assets/front/js/hoverIntent.js');?>"></script>
	<script src="<?php echo base_url ('assets/front/js/superfish.js');?>"></script>

	<!-- Main JS (Do not remove) -->
	<script src="assets/front/js/main.js"></script>

<!-- START sticky footer --><script src="assets/front/js/demoscript.js"></script><!-- END sticky footer -->

  <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-65003908-1', 'auto');
      ga('send', 'pageview');

    </script>

	</body>


</html>
