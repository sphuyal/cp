<?php $this->load->view('header'); ?> 
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css/fonts/login.css');?>" >
<div id="fh5co-wrapper">
<div id="fh5co-page">
<?php $this->load->view('menu'); ?>
<!-- end:fh5co-header -->
<div class="fh5co-hero" style="background-color:#0d0e11;height:100px">
</div>


<div class="container">
<div class="row">
        <div id="login-row" class="row justify-content-center align-items-center">
        
            <div id="login-column" class="col-md-6 col-sm-8 col-sm-offset-2 mx-auto col-md-offset-3"  style="margin-top: 100px;">
                <div class="login-box col-md-12" style="background-color: #f2f2f2;height:400px;margin-top: 50px;margin-bottom: 80px;">
                <form class="form-signin" action="<?php echo base_url('login/auth');?>" method="post">
           <h2 class="form-signin-heading"><strong>LOGIN</strong></h2>
           <?php echo $this->session->flashdata('msg');?>
           <label for="username" class="sr-only">Username</label>
           <input type="username" name="username" class="form-control" placeholder="Username" required>
           <label for="password" class="sr-only">Password</label>
           <input type="password" name="password" class="form-control" placeholder="Password" required style="margin-top: 30px;">
           <button class="btn btn-lg btn-primary btn-block" type="submit" style="margin-top: 40px;">Sign in</button>
           </form>
          </div>
            </div>
        </div>
    </div>
    </div>
</div>


<?php $this->load->view('footer'); ?> 
