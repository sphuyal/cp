
<?php $this->load->view('header'); ?>

<div id="fh5co-wrapper">
<div id="fh5co-page">
<?php $this->load->view('menu'); ?>
<!-- end:fh5co-header -->
<div class="fh5co-hero">
	<div class="fh5co-overlay"></div>
	<div class="fh5co-cover" data-stellar-background-ratio="0.5" style="background-image: url(images/fitness.jpg);">
		<div class="desc animate-box">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<h2>Fitness &amp; Health <br>is a <b>Mentality</b></h2>
						<p><span>Created with <i class="icon-heart3"></i> by the fine folks  </span></p>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end:fh5co-hero -->
<div id="fh5co-schedule-section" class="fh5co-lightgray-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="heading-section text-center animate-box">
					<h2>Class Schedule</h2>
					<p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
				</div>
			</div>
		</div>
		<div class="row animate-box">
			<div class="col-md-10 col-md-offset-1 text-center">
				<ul class="schedule">
					<li><a href="#" class="active" data-sched="sunday">Sunday</a></li>
					<li><a href="#" data-sched="monday">Monday</a></li>
					<li><a href="#" data-sched="tuesday">Tuesday</a></li>
					<li><a href="#" data-sched="wednesday">Wednesday</a></li>
					<li><a href="#" data-sched="thursday">Thursday</a></li>
					<li><a href="#" data-sched="friday">Friday</a></li>
					<li><a href="#" data-sched="saturday">Saturday</a></li>
				</ul>
			</div>
			<div class="row text-center">

				<div class="col-md-12 schedule-container">

					<div class="schedule-content active" data-day="sunday">
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-dumbell.svg" alt="Cycling">
								<small>06AM-7AM</small>
								<h3>Body Building</h3>
								<span>John Doe</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-yoga.svg" alt="">
								<small>06AM-7AM</small>
								<h3>Yoga Programs</h3>
								<span>James Smith</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-cycling.svg" alt="">
								<small>06AM-7AM</small>
								<h3>Cycling Program</h3>
								<span>Rita Doe</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-boxing.svg" alt="Cycling">
								<small>06AM-7AM</small>
								<h3>Boxing Fitness</h3>
								<span>John Dose</span>
							</div>
						</div>
					</div>
					<!-- END sched-content -->

					<div class="schedule-content" data-day="monday">
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-yoga.svg" alt="">
								<small>06AM-7AM</small>
								<h3>Yoga Programs</h3>
								<span>James Smith</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-dumbell.svg" alt="Cycling">
								<small>06AM-7AM</small>
								<h3>Body Building</h3>
								<span>John Doe</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-boxing.svg" alt="Cycling">
								<small>06AM-7AM</small>
								<h3>Boxing Fitness</h3>
								<span>John Dose</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-cycling.svg" alt="">
								<small>06AM-7AM</small>
								<h3>Cycling Program</h3>
								<span>Rita Doe</span>
							</div>
						</div>
						
					</div>
					<!-- END sched-content -->

					<div class="schedule-content" data-day="tuesday">
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-dumbell.svg" alt="Cycling">
								<small>06AM-7AM</small>
								<h3>Body Building</h3>
								<span>John Doe</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-yoga.svg" alt="">
								<small>06AM-7AM</small>
								<h3>Yoga Programs</h3>
								<span>James Smith</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-cycling.svg" alt="">
								<small>06AM-7AM</small>
								<h3>Cycling Program</h3>
								<span>Rita Doe</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-boxing.svg" alt="Cycling">
								<small>06AM-7AM</small>
								<h3>Boxing Fitness</h3>
								<span>John Dose</span>
							</div>
						</div>
					</div>
					<!-- END sched-content -->

					<div class="schedule-content" data-day="wednesday">
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-yoga.svg" alt="">
								<small>06AM-7AM</small>
								<h3>Yoga Programs</h3>
								<span>James Smith</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-dumbell.svg" alt="Cycling">
								<small>06AM-7AM</small>
								<h3>Body Building</h3>
								<span>John Doe</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-boxing.svg" alt="Cycling">
								<small>06AM-7AM</small>
								<h3>Boxing Fitness</h3>
								<span>John Dose</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-cycling.svg" alt="">
								<small>06AM-7AM</small>
								<h3>Cycling Program</h3>
								<span>Rita Doe</span>
							</div>
						</div>
					</div>
					<!-- END sched-content -->

					<div class="schedule-content" data-day="thursday">
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-dumbell.svg" alt="Cycling">
								<small>06AM-7AM</small>
								<h3>Body Building</h3>
								<span>John Doe</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-yoga.svg" alt="">
								<small>06AM-7AM</small>
								<h3>Yoga Programs</h3>
								<span>James Smith</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-cycling.svg" alt="">
								<small>06AM-7AM</small>
								<h3>Cycling Program</h3>
								<span>Rita Doe</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-boxing.svg" alt="Cycling">
								<small>06AM-7AM</small>
								<h3>Boxing Fitness</h3>
								<span>John Dose</span>
							</div>
						</div>
					</div>
					<!-- END sched-content -->

					<div class="schedule-content" data-day="friday">
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-yoga.svg" alt="">
								<small>06AM-7AM</small>
								<h3>Yoga Programs</h3>
								<span>James Smith</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-dumbell.svg" alt="Cycling">
								<small>06AM-7AM</small>
								<h3>Body Building</h3>
								<span>John Doe</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-boxing.svg" alt="Cycling">
								<small>06AM-7AM</small>
								<h3>Boxing Fitness</h3>
								<span>John Dose</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-cycling.svg" alt="">
								<small>06AM-7AM</small>
								<h3>Cycling Program</h3>
								<span>Rita Doe</span>
							</div>
						</div>
					</div>
					<!-- END sched-content -->

					<div class="schedule-content" data-day="saturday">
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-dumbell.svg" alt="Cycling">
								<small>06AM-7AM</small>
								<h3>Body Building</h3>
								<span>John Doe</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-yoga.svg" alt="">
								<small>06AM-7AM</small>
								<h3>Yoga Programs</h3>
								<span>James Smith</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-cycling.svg" alt="">
								<small>06AM-7AM</small>
								<h3>Cycling Program</h3>
								<span>Rita Doe</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="program program-schedule">
								<img src="images/fit-boxing.svg" alt="Cycling">
								<small>06AM-7AM</small>
								<h3>Boxing Fitness</h3>
								<span>John Dose</span>
							</div>
						</div>
					</div>
					<!-- END sched-content -->
				</div>

				
			</div>
		</div>
	</div>
</div>
<div class="fh5co-parallax" style="background-image: url(images/gym4.jpg);" data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 text-center fh5co-table">
				<div class="fh5co-intro fh5co-table-cell animate-box">
					<h1 class="text-center">Commit To Be Fit</h1>
					<p>Made with love by the fine folks  <a href=""></a></p>
				</div>
			</div>
		</div>
	</div>
</div><!-- end: fh5co-parallax -->
<div id="fh5co-programs-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="heading-section text-center animate-box">
					<h2>Our Programs</h2>
					<p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
				</div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-md-4 col-sm-6">
				<div class="program animate-box">
					<img src="images/fit-dumbell.svg" alt="Cycling">
					<h3>Body Combat</h3>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
					<span><a href="#" class="btn btn-default">Join Now</a></span>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="program animate-box">
					<img src="images/fit-yoga.svg" alt="">
					<h3>Yoga Programs</h3>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
					<span><a href="#" class="btn btn-default">Join Now</a></span>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="program animate-box">
					<img src="images/fit-cycling.svg" alt="">
					<h3>Cycling Program</h3>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
					<span><a href="#" class="btn btn-default">Join Now</a></span>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="program animate-box">
					<img src="images/fit-boxing.svg" alt="Cycling">
					<h3>Boxing Fitness</h3>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
					<span><a href="#" class="btn btn-default">Join Now</a></span>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="program animate-box">
					<img src="images/fit-swimming.svg" alt="">
					<h3>Swimming Program</h3>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
					<span><a href="#" class="btn btn-default">Join Now</a></span>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="program animate-box">
					<img src="images/fit-massage.svg" alt="">
					<h3>Massage</h3>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
					<span><a href="#" class="btn btn-default">Join Now</a></span>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="fh5co-team-section" class="fh5co-lightgray-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="heading-section text-center animate-box">
					<h2>Meet Our Trainers</h2>
					<p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
				</div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-md-4 col-sm-6">
				<div class="team-section-grid animate-box" style="background-image: url(images/mahesh.jpg);">
					<div class="overlay-section">
						<div class="desc">
							<h3>Mahesh Maharjan</h3>
							<span>Head Trainer</span>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
							<p class="fh5co-social-icons">
								<a href="#"><i class="icon-twitter-with-circle"></i></a>
								<a href="#"><i class="icon-facebook-with-circle"></i></a>
								<a href="#"><i class="icon-instagram-with-circle"></i></a>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="team-section-grid animate-box" style="background-image: url(images/a.jpg);">
					<div class="overlay-section">
						<div class="desc">
							<h3>Sukadev Karki</h3>
							<span>Trainer</span>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
							<p class="fh5co-social-icons">
								<a href="#"><i class="icon-twitter-with-circle"></i></a>
								<a href="#"><i class="icon-facebook-with-circle"></i></a>
								<a href="#"><i class="icon-instagram-with-circle"></i></a>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="team-section-grid animate-box" style="background-image: url(images/c.jpg);">
					<div class="overlay-section">
						<div class="desc">
							<h3>John Doe</h3>
							<span>Chief Executive Officer</span>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="team-section-grid animate-box" style="background-image: url(images/b.jpg);">
					<div class="overlay-section">
						<div class="desc">
							<h3>John Doe</h3>
							<span>Chief Executive Officer</span>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="team-section-grid animate-box" style="background-image: url(images/nanita.jpg);">
					<div class="overlay-section">
						<div class="desc">
							<h3>Nanita Maharjan</h3>
							<span>Chief Executive Officer</span>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="team-section-grid animate-box" style="background-image: url(images/couple.jpg);">
					<div class="overlay-section">
						<div class="desc">
							<h3> Rajesh and Namrata</h3>
							<span>Couple Trainer</span>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>
<div class="fh5co-parallax"  style="background-image: url(images/gym5.jpg);" data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 col-md-pull-3 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 fh5co-table">
				<div class="fh5co-intro fh5co-table-cell box-area">
					<div class="animate-box">
						<h1>Pay now and get 25% Discount</h1>
						<p>Know more about us!</p>
						<a href="<?php echo base_url('about'); ?>" class="btn btn-primary">About US</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- end: fh5co-parallax -->



<?php $this->load->view('footer'); ?>