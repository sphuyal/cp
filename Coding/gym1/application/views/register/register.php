<?php $this->load->view('header'); ?> 
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css/register.css');?>">
<div id="fh5co-wrapper">
<div id="fh5co-page">
<?php $this->load->view('menu'); ?>
<!-- end:fh5co-header -->
<div class="fh5co-hero" style="background-color:#0d0e11;height:100px">
</div>


<div class="register1">
<div id="register" class="register">
<div class="hello">
<form class="form-horizontal" method="post"  role="form">
                <h2 style="text-align:center;">Registration</h2>
                <?php
		        echo form_open('register/register/index');
		        echo validation_errors();
		        if (isset($success))
		        echo '<p>'.$success.'</p>';
	            ?>
                <div class="form-group">
                    <label for="name" class="col-sm-4">Name</label>                             
                    <div class="col-sm-12">
                        <input type="text" id="" placeholder="Name" name="name" class="form-control" autofocus>
                    </div>
                </div>
                <div class="form-group">
                            <label for="class" class="col-sm-4">Choose</label>
                         <div class="col-sm-12">
                            <select class="form-control form-control-sm" name="choose" id="choose">
                            <option value="" selected>Select</option>
                            <option>Customer</option>
                            </select>
                        </div>
                </div>

                



                <div class="form-group">
    
                  <label class="col-sm-2"  for="gender">Gender:</label>
                      <input type="radio" name="gender" id="male" value="male" />
                  <label for="male" class="opt">Male</label>
                   <input type="radio" name="gender" id="female" value="female" />
                      <label for="female" class="opt">Female</label>
                    <input type="radio" name="gender" id="other" value="other" />
                  <label for="other" class="opt">Other</label>
                 </div> 





                <div class="form-group">
                    <label for="address" class="col-sm-4">Address</label>
                    <div class="col-sm-12">
                        <input type="text" id="address" placeholder="Address" name="address" class="form-control" autofocus>
                    </div>
                </div>


                <div class="form-group">
                    <label for="phoneNumber" class="col-sm-4">Phone Number </label>
                    <div class="col-sm-12">
                        <input type="phoneNumber" id="phoneNumber" placeholder="Phonenumber" name="phonenumber" class="form-control">
                    </div>
                </div>



            
                

     
                
                <div class="form-group">
                    <label for="email" class="col-sm-4">Email</label>
                    <div class="col-sm-12">
                        <input type="email" id="email" placeholder="Email" class="form-control" name= "email">
                    </div>
                </div>

                <div class="form-group">
                    <label for="username" class="col-sm-4">Username</label>
                    <div class="col-sm-12">
                        <input type="text" id="username" placeholder="Username" name="username" class="form-control" autofocus>
                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="col-sm-4">Password*</label>
                    <div class="col-sm-12">
                        <input type="password" id="password" placeholder="Password"    name="password" class="form-control">
                    </div>
                </div>

                       

                
            
                <button type="submit" name="register_submit" class="btn btn-primary btn-block">Register</button>
                <?php 
                echo form_close(); 
                ?>
                </form>
    </div> 
</div>
</div>
<?php $this->load->view('f'); ?>

