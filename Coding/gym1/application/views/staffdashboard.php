
<?php $this->load->view('header'); ?> 
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css/register.css');?>">

<div class="register1">
<div id="register" class="register">
<div class="hello">
<form class="form-horizontal" method="post" role="form" action="<?php echo base_url()?>Staff/edit">   
                <h2 style="text-align:center;"><?php echo $this->session->userdata('name');?> Profile</h2>
                <?php 
                if($this->session->flashdata('update')) 
                {
                echo '<div class="alert alert-warning">';
                echo $this->session->flashdata('update');
                echo "</div>";
                }
                ?>
               
                 
                <div class="form-group">
                    <label for="name" class="col-sm-4">Name</label>
                    <div class="col-sm-12">
                        <input type="text" id="" placeholder="Name" name="name" value="<?php echo $detail->name?>" class="form-control" autofocus />
                      
                    </div>
                </div>
                
                



                <div class="form-group">
                    <label for="name" class="col-sm-4">Gender:</label>
                    <div class="col-sm-12">
                        <input type="text" id="" placeholder="gender" name="gender" value="<?php echo $detail->gender?>" class="form-control" autofocus />
                      
                    </div>
                </div>





                <div class="form-group">
                    <label for="address" class="col-sm-4">Address</label>
                    <div class="col-sm-12">
                        <input type="text" id="address" placeholder="Address" value="<?php echo $detail->address?>" name="address" class="form-control" autofocus>
                    </div>
                </div>


                <div class="form-group">
                    <label for="phoneNumber" class="col-sm-4">Phone Number </label>
                    <div class="col-sm-12">
                        <input type="phoneNumber" id="phoneNumber" placeholder="Phone number"   value="<?php echo $detail->phonenumber?>" name="phonenumber" class="form-control">
                    </div>
                </div>



            
                

     
                
                <div class="form-group">
                    <label for="email" class="col-sm-4">Email</label>
                    <div class="col-sm-12">
                        <input type="email" id="email" placeholder="Email" class="form-control" name= "email" value="<?php echo $detail->email?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="username" class="col-sm-4">Username</label>
                    <div class="col-sm-12">
                        <input type="text" id="username" placeholder="Username" name="username" class="form-control" autofocus value="<?php echo $detail->username?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="col-sm-4">Password*</label>
                    <div class="col-sm-12">
                        <input type="text" id="password" placeholder="Password"    name="password" class="form-control" value="<?php echo $detail->password?>">
                    </div>
                </div>

                <button type="update" name="update" class="btn btn-primary btn-block" value="Update" >Update</button>
                <?php echo form_close(); ?>
               
               
                </form>
               
    </div> 
</div>
</div>








