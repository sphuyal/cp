<?php $this->load->view('header'); ?>
	<div id="fh5co-wrapper">
		<div id="fh5co-page">
		<?php $this->load->view('menu'); ?>
		<!-- end:fh5co-header -->
		<div class="fh5co-parallax" style="background-image: url(images/protein.jpg);" data-stellar-background-ratio="0.5">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 text-center fh5co-table">
						<div class="fh5co-intro fh5co-table-cell animate-box">
							<h1 class="text-center">Product Detail</h1>
							<p>Help to be Fit!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- notation -->
		
		<div id="fh5co-team-section">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="heading-section text-center animate-box">
							<h2>Come and Take products</h2>
							<p>Our products quality is one of the best in this country.</p>
						</div>
					</div>
				</div>
				<div class="row text-center">
					<div class="col-md-4 col-sm-6">
						<div class="team-section-grid animate-box" style="background-image: url(images/gain.jpg);">
							<div class="overlay-section">
								<div class="desc">
									<h3>Protein Shake</h3>
									<span>Price: RS. 1000</span>
									<p>It helps to lower weight person to make body</p>
									
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="team-section-grid animate-box" style="background-image: url(images/boxing.jpg);">
							<div class="overlay-section">
								<div class="desc">
									<h3>Boxing Gloves</h3>
									<span>Gloves for boxers</span>
									<p> To provide a secure and snug fit</p>
									
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="team-section-grid animate-box" style="background-image: url(images/wrap.jpg);">
							<div class="overlay-section">
								<div class="desc">
									<h3>Wrap</h3>
									<span>Ankle Wrap</span>
									<p>We provides high-quality poly cotton</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="team-section-grid animate-box" style="background-image: url(images/hand.jpg);">
							<div class="overlay-section">
								<div class="desc">
									<h3>Wraps</h3>
									<span>Hand Wraps</span>
									<p>hand wraps are always used under boxing or training gloves for ultimate wrist and knuckle protection.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="team-section-grid animate-box"  style="background-image: url(images/boxing2.jpg);">
							<div class="overlay-section">
								<div class="desc">
									<h3>Shin Guard</h3>
									<span>Fairtex Shin Guard</span>
									<p>These can be used in amateur competition as there are no metal loops.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="team-section-grid animate-box" style="background-image: url(images/guard.jpg);">
							<div class="overlay-section">
								<div class="desc">
									<h3>Groin Protector</h3>
									<span>Twins Special Groin Protector</span>
									<p>Designed with three laces for secure and comfortable protection.</p>
								</div>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>
		
					
					
					
<?php $this->load->view('footer'); ?>