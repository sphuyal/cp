<?php $this->load->view('header'); ?> 
<h2 align="center"><?= $title ?></h2>
<?php foreach($events as $event) : ?>
<div class="container">
        <div class="row">
            <div class="alert alert-warning" style="background-color:#5edcb2;">
                <u>
                    <div class="col-md-6"><b>Event-Date: <?php echo $event['event_date']?></b></div>
                    <div class="col-md-6"><span class="pull-right"><b>Event-Time: <?php echo $event['event_time']?></b></span></div>
                </u> <br>
                <div class="alert alert-success">
                <label><b>Event: <u><?php echo $event['title']?></u></b></label>
                </div>
                <div class="alert alert-info">
                    <h4 align="justify"><?php echo substr($event['body'],0,300)?> </h4>
                </div>
                <p><a class="btn btn-success" href="<?php echo site_url('/viewevent/'.$event['slug']); ?>"> Read More</a></p>
            </div>

        </div>
    </div>

<?php endforeach; ?>
