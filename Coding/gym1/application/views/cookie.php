
<link rel="stylesheet" href="<?php echo base_url('assets/css/css.css');?>">

<?php
if(isset($_GET['accept_cookie']))
{
    setcookie('allow_cookie','true',time()+60*60*24*30);
    header("Location: ./");
}

if(!isset($_COOKIE['allow_cookie']))
{
    ?>
<div class="accept_cookie">
    <div class="container">
        <p>We use cookies on this website. By using this website, we'll assumes you consent to <a href="" style="color: gold;">the cookies we set</a></p>
        <p>
            <a href="?accept_cookie" class="btn btn-outline-light" style="background-color:aliceblue;">Ok, Continue</a>
        </p>

    </div>

</div>
<?php
}
?>