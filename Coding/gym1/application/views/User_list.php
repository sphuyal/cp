<div class="container" >


<div class="row " >
<h2  class="alert alert-light text-center">User List</h2>
        <div class="col-lg-8 mt-12"  >
            <form action="" method="get">
        
                            <div class="input-group col-md-12">
                                <input type="text" value="<?php if(isset($_GET['search_input'])){echo $_GET['search_data'];} ?>"  id="search_data" name="search_data"  class="form-control" placeholder="Search" />
                                <span class="input-group-btn">
                                    <button class="button button-green" type="submit">
                                        <span class=" glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                            </div>
            
            
        </form>
                    
        </div>
          <div class="col-lg-2 ">
              <button  class="button button-purple mt-12 pull-right" data-toggle="modal" data-target="#create_user_info_modal"> Create Student</button> 
      
          </div>
         
    </div>
         <?php 
        
        if($this->session->flashdata('message')){
                echo "<p class='custom-alert'>".$this->session->flashdata('message');"</p>";
        // unset($_SESSION['message']);
       }
     
        ?>
<table class="table" >
            <thead>
                <tr>
             
                    
                    <th>Name</th>
                    <th>Choose</th>
                    <th>Email</th>
                    <th>Gender</th>
                    <th>Address</th>
                    <th>Phonenumber</th>
                    <th>Action</th>




                </tr>
            </thead>
            <tbody>
                
          
                <?php

                if(isset($student_list)){
                    foreach ($student_list as $key => $value) {
                        
                ?>

                <tr >
                   
                    
                    <td><?php echo  $value['name'];?></td>
                    <td><?php echo  $value['choose'];?></td>
                    <td><?php echo  $value['email'];?></td>
                    <td><?php echo  $value['gender'];?></td>
                    <td><?php echo  $value['address'];?></td>
                    <td><?php echo  $value['phonenumber'];?></td>
                    
                 
                <td>
                 

                    <a  href="<?php echo site_url('view-user-info').'/'.$value['id'];?>"   class="button button-green" >View</a> 
                    <a href="<?php echo site_url('update-user-info').'/'.$value['id'];?>"   class="button button-blue" >Edit</a> 
                    <a href="<?php echo site_url('delete-user-info').'/'.$value['id'];?>"    class="button button-red" >Delete</a> 
                  
                   
                  
                    

                    
                
                </td>
                    
                    
                    
                </tr>
                
                <?php
                
                    }
                }
                ?>
                

           </tbody>
        </table>
    

<div class="pull-right">
 
   <?php if(isset($pagination)) {
       echo $pagination;
                
        }
        ?>
   
</div>


<div class="modal fade" id="create_user_info_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create New Account</h4>
        </div>
        <div class="modal-body">
         
            <form method="post"  id="create_user_info" action="<?php echo site_url('Detail/create_user_info'); ?>" >
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" name="name" id="name" class="form-control" required maxlength="50">
            </div>


            <div class="form-group">
                <label for="choose">Choose:</label>
                <select class="form-control" name="choose" id="choose">
                    <option value="" selected>Select</option>
                    <option value="staff" >Trainer</option>
                    <option value="trainer" >Staff</option>
                    <option value="customer" >Customer</option>
                </select>
            </div> 

            <div class="form-group">
                <label for="email">Email address:</label>
                <input type="email" class="form-control" name="email" id="email" required maxlength="50">
            </div>

            <div class="form-group">
                <label for="contact">Contact:</label>
                <input type="text" class="form-control" name="phonenumber" id="phonenumber"  maxlength="50">
            </div>

            <div class="form-group">
                <label for="gender">Gender:</label>
                <select class="form-control" name="gender" id="gender">
                    <option value="" selected>Select</option>
                    <option value="male" >Male</option>
                    <option value="female" >Female</option>
                    <option value="other" >Other</option>
                </select>
            </div> 

            <div class="form-group">
                <label for="address">Address:</label>
                <input type="text" name="address" name="address" id="address" class="form-control"  maxlength="50">
            </div>

            <div class="form-group">
                <label for="address">Usernme:</label>
                <input type="text" name="username" name="username" id="username" class="form-control"  maxlength="50">
            </div>

            <div class="form-group">
                <label for="address">Password:</label>
                <input type="text" name="password" name="password" id="password" class="form-control"  maxlength="50">
            </div>


                <div class="form-group mb-50">
            <input type="submit" class="button button-green  pull-right"  value="Submit"/>
                </div>
                
        </form> 
    
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div> 
               
                  

 


