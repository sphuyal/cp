<?php $this->load->view('header'); ?>
	<div id="fh5co-wrapper">
		<div id="fh5co-page">
		<?php $this->load->view('menu'); ?>
		<!-- end:fh5co-header -->
		<div class="fh5co-parallax" style="background-image: url(images/location.jpg);" data-stellar-background-ratio="0.5">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 text-center fh5co-table">
						<div class="fh5co-intro fh5co-table-cell animate-box">
							<h1 class="text-center">Contact Us</h1>
							<p>Any Problem! Call Me<a></a></p>
						</div>
					</div>
				</div>
			</div>
		</div><!-- end: fh5co-parallax -->
		<!-- end:fh5co-hero -->
		<div id="fh5co-contact">
			<div class="container">
				<form action="#">
					<div class="row">
						<div class="col-md-6 animate-box">
							<h3 class="section-title">Our Address</h3>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
							<ul class="contact-info">
								<li><i class="icon-location-pin"></i>Near BID Gate, Balaju</li>
								<li><i class="icon-phone2"></i>+9779860486798</li>
								<li><i class="icon-mail"></i><a href="#"><span class="__cf_email__" data-cfemail="e28b8c848da29b8d9790918b9687cc818d8f">sphuyal0@gmail.com</span></a></li>
								<li><i class="icon-globe2"></i><a href="#">www.yoursite.com</a></li>
							</ul>
						</div>
						<div class="col-md-6 animate-box">
							<div class="row">
							<div id="googleMap" style="width:100%;height:400px;margin-bottom: 100px;" align="center">
                               <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3531.6209627806084!2d85.30199631496019!3d27.728986982783066!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjfCsDQzJzQ0LjQiTiA4NcKwMTgnMTUuMSJF!5e0!3m2!1sen!2snp!4v1560520283396!5m2!1sen!2snp" width="700" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                             </div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- END fh5co-contact -->
		
<?php $this->load->view('footer'); ?>
