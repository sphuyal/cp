<?php $this->load->view('header'); ?> 
    <div class="container-fluid">
      <div class="row">
      <nav class="navbar navbar-default" style="background-color: black; color:white">
          <div class="container-fluid" style="margin-top: 20px;">
            <div class="navbar-header" style="margin-right: 40px;">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="ma">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
           
						<h1 id="fh5co-logo"><a href="">Fit<span>ness</span></a></h1>
            </div>
            <div id="navbar" class="navbar-collapse collapse" style="margin-bottom: 15px; color:#fff">
              <ul class="nav navbar-nav" style="">
                <!--ACCESS MENUS FOR ADMIN-->
                <?php if($this->session->userdata('choose')==='admin'):?>
                  <li><a href="<?php echo base_url('admindashboard'); ?>">Profile</a></li>
                  <li><a href="<?php echo base_url('detail'); ?>">Others Profile</a></li>
                  <li><a href="<?php echo base_url('Eventpost'); ?>">Post Events</a></li>
                  <li><a href="<?php echo base_url('event'); ?>">View Events</a></li>
                  <li><a href="<?php echo base_url('complainview'); ?>">View Complaints</a></li>
                <!--ACCESS MENUS FOR STAFF-->
                <?php elseif($this->session->userdata('choose')==='staff'):?>
                  <li><a href="<?php echo base_url('staffdashboard'); ?>">Profile</a></li>
                  <li><a href="<?php echo base_url('viewevent'); ?>">View Event</a></li>
                  <li><a href="<?php echo base_url('Eventpost'); ?>">Post Events</a></li>
                   <!--ACCESS MENUS FOR STAFF-->
                <?php elseif($this->session->userdata('choose')==='trainer'):?>
                  <li><a href="<?php echo base_url('trainerdashboard'); ?>">Profile</a></li>
                  <li><a href="<?php echo base_url('Eventpost'); ?>">Post Events</a></li>
                  <li><a href="<?php echo base_url('viewevent'); ?>">View Event</a></li>
                <!--ACCESS MENUS FOR AUTHOR-->
                <?php else:?>
                  <li><a href="<?php echo base_url('customerdashboard'); ?>">Profile</a></li>
                  <li><a href="<?php echo base_url('viewevent'); ?>">View Event</a></li>
                  <li><a href="<?php echo base_url('complains'); ?>">Complaints</a></li>
                <?php endif;?>
              </ul>
              <ul class="nav navbar-nav navbar-right">
              <li><a>Hello_<?php echo $this->session->userdata('choose');?></a></li>
                <li ><a href="<?php echo site_url('login/logout');?>" ><span>Signout</span></a></li>
        


              </ul>
            </div><!--/.nav-collapse -->
          </div><!--/.container-fluid -->
        </nav>
 
       
 
      </div>
    </div>
    <script src="<?php echo  base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>
    
  


