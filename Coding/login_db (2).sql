-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 09, 2019 at 01:31 AM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `login_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_complains`
--

CREATE TABLE `tbl_complains` (
  `c_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `complains` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_complains`
--

INSERT INTO `tbl_complains` (`c_id`, `id`, `complains`, `date`) VALUES
(3, 5, 'Whether it’s eating better or starting a new workout, you will have a client who has doubts about their abilities or just don’t want to work at it. If your client is overweight or recovering from an illness or surgery, they may be skeptical that working m', '2019-07-02 16:22:52'),
(4, 11, 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one ', '2019-07-02 17:45:47'),
(5, 9, 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.\r\n\r\n', '2019-07-02 17:49:48'),
(6, 9, 'hello guys\r\n', '2019-07-03 15:49:21'),
(7, 9, 'In  my system their is a problem. Plz fix it', '2019-07-25 06:52:52'),
(8, 21, 'mzjvnjksdz', '2019-08-04 05:34:28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_event`
--

CREATE TABLE `tbl_event` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `event_date` date NOT NULL,
  `event_time` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_event`
--

INSERT INTO `tbl_event` (`id`, `title`, `slug`, `body`, `event_date`, `event_time`) VALUES
(16, 'Event in putalisadak', 'event-in-putalisadak', 'Enhanced Vocational Education and Training (EVENT) project second has been established as per the agreement signed between the Government of Nepal and World Bank on 21 January, 2018. The main objective of this project is to expand the supply of skilled and employable labor by increasing access to quality training programs, and by strengthening the technical and vocational education and training system in Nepal. The project emphasizes in increasing access to technical education and vocational training (TEVT) programs for disadvantaged youth especially poor, living in lagging regions, female, Dalit, marginalized Janajatis and people with disability through targeting and other inclusive processes.', '2019-09-06', '10am To 2pm'),
(17, 'Event in banasthali', 'event-in-banasthali', 'Enhanced Vocational Education and Training (EVENT) project second has been established as per the agreement signed between the Government of Nepal and World Bank on 21 January, 2018. The main objective of this project is to expand the supply of skilled and employable labor by increasing access to quality training programs, and by strengthening the technical and vocational education and training system in Nepal. The project emphasizes in increasing access to technical education and vocational training (TEVT) programs for disadvantaged youth especially poor, living in lagging regions, female, Dalit, marginalized Janajatis and people with disability through targeting and other inclusive processes.', '2019-09-06', '10am To 6pm'),
(18, 'near balaju', 'near-balaju', 'Hello\" is a song by British singer-songwriter Adele, released on 23 October 2015 by XL Recordings as the lead single from her third studio album, 25 (2015). Adele co-wrote the song with her producer, Greg Kurstin. \"Hello\" is a piano ballad with soul influences, and lyrics that discuss themes of nostalgia and regret. Upon release, the song was acclaimed by music critics, who compared it favourably to Adele\'s previous work and praised the song\'s lyrics and Adele\'s vocals. It was recorded in London\'s Metropolis Studios.\r\n\r\n\"Hello\" reached number one in almost every country it charted in, including the United Kingdom, where it became her second chart topper, following \"Someone Like You\", and has the largest opening week sales in three years. In the United States, \"Hello\" debuted at the top of the Billboard Hot 100, reigning for 10 consecutive weeks whilst becoming Adele\'s fourth number-one on the chart and breaking several records, including becoming the first song to sell over a million digital copies in a week. By the end of 2015, it had sold 12.3 million units globally (combined sales and track-equivalent streams) and was the year\'s 7th best-selling single while it stands as one of the best-selling singles of all-time.[3]', '2019-06-12', '2pm-5pm'),
(19, 'Event in putalisadk', 'event-in-putalisadk', 'manfhaksjfnajf', '2019-08-01', '00..00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subscribe`
--

CREATE TABLE `tbl_subscribe` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_subscribe`
--

INSERT INTO `tbl_subscribe` (`id`, `email`) VALUES
(2, 'fikrifiver97@gmail.com'),
(6, 'gita12@gmail.com'),
(7, 'sphuyal0@gmail.com'),
(8, 'phuyals31@gmail.com'),
(9, 'san@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `choose` enum('admin','staff','trainer','customer') DEFAULT NULL,
  `gender` enum('male','female','other','') NOT NULL,
  `address` varchar(50) NOT NULL,
  `phonenumber` bigint(15) NOT NULL,
  `username` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `name`, `email`, `password`, `choose`, `gender`, `address`, `phonenumber`, `username`) VALUES
(1, 'Sandesh  Phuyal', 'sphuyal0@gmail.com', '123', 'admin', 'male', 'Balaju', 9860486798, 'fik'),
(2, 'Daria', 'email2@gmail.com', '345', 'staff', 'male', 'banasthali', 9860486798, 'email2'),
(5, 'Gita phuyal', 'gita123@gmail.com', '123', 'customer', 'female', 'balaju', 9860486798, 'gita1'),
(6, 'sandesh', 'phuyals31@gmail.com', 'kingdom123', 'trainer', 'male', 'balaju', 9851005559, 'phuyal31'),
(7, 'dharma', 'gita12@gmail.com', 'kingdom123', 'staff', 'male', 'banasthali', 9741035045, 'gita'),
(9, 'prawin ayadi', 'prawin1@gmail.com', 'kingdom123', 'customer', 'male', 'dillibazzar', 9851005559, 'prawin'),
(10, 'pukar ', 'pukar1@gmail.com', 'kingdom123', 'staff', 'male', 'banasthali', 9741035045, 'pukar'),
(11, 'Mark Zukerburg', 'mark1@facebook.com', 'kingdom123', 'customer', 'male', 'Newwork', 787462786434, 'mark1'),
(12, 'Dawa', 'dawa@gmail.com', 'dawa111', 'customer', 'male', 'Boudha', 9866442858, 'dawa'),
(13, 'Dhiraj Phuyal', 'dhiraj1@gmail.com', 'kinglion123', 'staff', 'male', 'bypass', 9851005559, 'dhiraj'),
(16, 'sitaram', 's0@gmail.com', 'kingdom123', 'staff', 'male', 'balaju', 9851005559, 's0'),
(17, 'pukar mishra', 'p1@gmail.com', 'kingdom123', 'customer', 'male', 'balaju', 9843411909, 'p1'),
(18, 'sahil mahar', 'sahil@gmail.com', 'kingdom123', 'customer', 'male', 'balaju', 9741035045, 'sahil'),
(20, 'Anish Dhakal', 'a@gmail.com', 'kingdom', 'customer', 'male', 'kavesthali', 9860486798, 'a1'),
(21, 'Anish Dhakal', 'a123@gmail.com', 'kingdom123', 'customer', 'male', 'kavesthali', 0, 'a123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_complains`
--
ALTER TABLE `tbl_complains`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `tbl_event`
--
ALTER TABLE `tbl_event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_subscribe`
--
ALTER TABLE `tbl_subscribe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_complains`
--
ALTER TABLE `tbl_complains`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_event`
--
ALTER TABLE `tbl_event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tbl_subscribe`
--
ALTER TABLE `tbl_subscribe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
